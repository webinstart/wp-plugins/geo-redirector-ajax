<?php
/*
Plugin Name: Geo Redirect Ajax
Description: Detects user's country and language and redirects them to a different path based on their location
Plugin URI: https://webinstart.com/
Version: 1.0.0
Author: Webinstart
Author URI: https://webinstart.com/
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/


function my_custom_js()
{

    // Our list of URLs
    $availableCountries = [
        "TR" => "/tr/",
        "FR" => "/fr/",
        "BE" => "/be/",
        "CH" => "/ch/",
        "DE" => "/de/",
        "AT" => "/at/",
        // "EN" => "/",
        // "US" => "/",
    ];

    $geoApiKey = "b883303c-caf8-458d-bb31-81663b55bc8c"; // to be set from WP backend
    $apiProvider = "https://apis.webinify.dev/geolocation/v1/geolocate?key=$geoApiKey"; // dev
    // $url = "https://apis.webinify.com/geolocation/v1/geolocate?key=$apiKey";

?>
    <script type="text/javascript">
        const countries = <?php echo json_encode($availableCountries) ?>;

        const isGeoRedirected = localStorage.getItem("geo_redirected");

        const geoRedirectorRequest = async (onSuccess = (json) => {}, onError = (res) => {}) => {
            const response = await fetch("<?php echo $apiProvider ?>", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                     "accept": "application/json"
                }
            });

            if (response.ok) {
                const dataJson = await response.json();
                onSuccess(dataJson);
            } else {
                onError(response);
            }

        }

        console.log("Geo already redirected", isGeoRedirected);

        if (isGeoRedirected !== "true") {
            geoRedirectorRequest(
                (json) => {
                    const countryCode = json.data.countryCode;

                    if (countries.hasOwnProperty(countryCode)) {
                        const langToRedirect = countries[countryCode];
                        console.log("Geo to redirect", langToRedirect);

                        let url = new URL(window.location.href);
                        url.pathname = langToRedirect;

                        const currentDate = new Date();
                        localStorage.setItem("geo_redirected", "true");
                        localStorage.setItem("geo_redirected_to", url.href);
                        localStorage.setItem("geo_redirected_date", currentDate.toJSON());

                        window.location.href = url.href;
                    }
                },
                (res) => {
                    console.log("Couldn't geo-locate");
                    console.log(res);

                })


        }
    </script>
<?php
}


function geo_redirect()
{

    // // Check if the user is in wp-admin
    if (is_admin()) {
        return;
    }

    // Get the current URL
    $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    // Parse the URL
    $parsed_url = parse_url($current_url);
    // Get the path
    $url_path = $parsed_url['path'];

    if ($url_path == "/") {

        add_action('wp_head', 'my_custom_js');
    }
}
add_action('init', 'geo_redirect');
